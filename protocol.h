//
// Created by Marouane BENALLA on 08/02/2017.
//

#ifndef SERVER_LIBRARY_PROTOCOL_H
#define SERVER_LIBRARY_PROTOCOL_H
/*
 * We define all the protocol communication base
 * this definitions are mine, so they don't follow any conventions.
 */

//Discover Protocol
#define DISCOVER 0x0820
#define DISCOVER_ACCEPT 0x0821
//Leader Protocol
#define INIT_LEADER_ELECTION 0x0840
#define LEADER_ACCEPT 0X0841
#define LEADER_REJECT 0X0842
//HeartBeat
#define STILL_LIVE 0x0860
#define HEART_BEAT 0X861
#define MEMORY_STATUS 0x862

#endif //SERVER_LIBRARY_PROTOCOL_H
