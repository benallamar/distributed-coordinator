//
// Created by Marouane BENALLA on 03/02/2017.
//
#include <iostream>
#include <sstream>
#include "node/node.h"
#include "parser/params_parser.h"

using namespace ServerAbstraction;

int main(int argc, char *argvs[]) {
    /*
     * Initiate a params parser
     */
    ConfigParams *params = new ConfigParams(argc, argvs);
    Node *node = new Node(params);
    /*
     * Remove an unnecessary object
     */
    delete (params);
    /*
     * Initiate the node
     */
    node->start();
}