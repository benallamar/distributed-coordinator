//
// Created by Marouane BENALLA on 10/02/2017.
//

#ifndef SERVER_LIBRARY_NODE_H

#include <iostream>
#include <vector>
#include <map>
#include "../transport/server_abstraction.h"
#include "../parser/params_parser.h"
#include "../protocol.h"
#include "../transport/http_entity.h"

#define SERVER_LIBRARY_NODE_H


class Node : public ServerAbstraction::Client {
public:
    Node();

    Node(std::string *, std::string *);

    Node(HTTPEntity *);

    Node(ConfigParams *);

    /*
     * Initiate all the interesting configuration an states to make the machine in
     * an admissible execution.
     */
    void start();

    /*
     * Have information about the neighbors, this methods is more simple than `createRoadMap' as it just collects
     * informatons about the neighbors.
     */
    void discover();

    /*
     *  Synchronize the information between the nodes, the idea is to use the anti-Entropy algorithms.
     */
    void synchronize(Node *);

    /*
     * Coordinator could be any node in the LAN,
     * The election of the coordinator could be
     */
    void initElection();

    /*
     * The creation of the road map, will helps the node to get information about
     * how far the others nodes in the LAN are far from it.
     */
    void createRoadMap();

    /*
     * Manage jobs: The aim of this methods is to make the node self-managed.
     * Update this functions if you want to have a special management of the local jobs
     */
    void manageJobs();

    /*
     * Set the neighborhood
     */
    void setNeighbor(Node *);

    /*
     * Set a map for a node
     */
    void setNodeMap(Node *, Node *);

    /*
     * Choose the color
     */
    void setColor(std::string);

    /*
     * Display color choice
     */
    void displayColor();

    /*
     * Check if a node cold be
     */
    bool isLeader(Node *);

    /*
     * Set the master
     */
    bool setMaster(Node *);

    /*
     * Report the status of the current node and
     */
    void sendReport(void *);

    /*
     * Broadcast message send
     */
    Neighbors *broadcast();

    /*
     * Handle the incoming request
     */
    void httpEntityHandler(HTTPEntity *);

    /*
     * Method to handle the httpEntityResponse
     */
    void discoverHandler(HTTPEntity *);

    void discoverAcceptHandler(HTTPEntity *);

    void initLeaderElectionHAndler(HTTPEntity *);

    void leaderAcceptHandler(HTTPEntity *);

    void leaderRejectHandler(HTTPEntity *);

    void heartBeatHAndler(HTTPEntity *);

    void memoryStatusHandler(HTTPEntity *);

private:
    /*
     * All the informations about the others nodes
     */
    typedef std::vector<Node *> Neighbors;
    Neighbors *neighbors_list;
    /*
     * We use the algorithms of Dijkistra to get the shortest path to a node
     */
    std::map<Node *, Node *> *road_map;
    /*
     * Every node starts by being a normal one, the first one to discover that no master has been elected will
     * send `Election candidat request` to every node on the local area network. The first request send will be
     * the one accepted, so it's first ask first algorithms.
     * The question is : How should we fix the consensus problem with many nodes selected different nodes.
     */
    Node *master;
    bool is_master = false;
    std::string color = "000000";
    /*
     * Every node has two unique identifiers,
     *  ip-address: which is the internet protocol address
     *  hash-address: which is the node's address in the ring
     */
    std::string *ip_address, *hash_address;
};


#endif //SERVER_LIBRARY_NODE_H
