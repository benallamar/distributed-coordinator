//
// Created by Marouane BENALLA on 10/02/2017.
//

#include "node.h"

Node::Node() {
/*
 * Initiate the node
 */
    super();
}

Node::Node(ConfigParams *params) {
    //To do some things with the configuration params

}

Node::Node(HTTPEntity *http_entity) {
    ip_address = http_entity->getIPAddress();
    hash_address = http_entity->getHashAddress();
}

void Node::discover() {
    /*
     * For every given response from the broadcast
     */
    HTTPEntity *http_body;
    std::string *response;
    while (http_body = acceptClient()) {
        while (http_body->isAlive()) {
            httpEntityHandler(http_body);
        }
    }
}

void Node::broadcast() {
    /*
     * Send a broadcast to every address
     */
}

void Node::setNeighbor(Node *neighbor) {
    neighbors_list->push_back(neighbor);
}

void Node::synchronize(Node *node) {
    //We start by getting the node hash_address
    road_map->find(node);
}

void Node::initElection() {
    /*
     * We start by filling up a candidature for the leader election
     * If the node hasn't receive any submission it will send the request to all her neighbors
     */
    Node *node;
    Neighbors::iterator i = neighbors_list->begin();
    /*
     * Compare with all lise
     */
    std::for_each(neighbors_list->begin(), neighbors_list->end(), setMaster);
}

void Node::createRoadMap() {
/*
 * Create a road-map of the local network.
 * We'll use the fact that every node, is more smart to collect information about her neighbors
 */
    setNodeMap(this, this);
}

void Node::manageJobs() {
    /*
     * Here we implement the algorithm of job management, make sure to not do some things wrong to your job
     * or not create any security issue.
     *  Initial algorithms:
     *      The implemented algorithms will be mini-coloriage.
     */
    /*
     * This is the part so complicated in the code
     * Please try to manage it very carefully, if you are not an expert
     * in Unix systems don't play with it to not damage your system
     */
}

void Node::setColor(std::string color) {
    this->color = color;
}

void Node::displayColor() {
/*
 * Set a visual display of the color
 */
}

bool Node::isLeader(Node *node) {
    return hash_address->compare(node->hash_address) > 0;
}

bool Node::setMaster(Node *neighbor) {
    if (neighbor->isLeader(this)) {
        /*
         * We compare the hash_address, the further node from the start of the ring is the coordinator(or the mayer)
         */
        master = neighbor;
    }
}

void sendReport(HTTPEntity *request) {
    /*
     * All the status should be send in a periodically way to the
     * Cassandra base for the persistent of the information.
     */
    request->send();

}

void Node::httpEntityHandler(HTTPEntity *httpEntity) {
    switch (httpEntity->status()) {
        case DISCOVER:
            /*
             * Handle the discover operation
             */
            discoverHandler(httpEntity);
            break;
        case DISCOVER_ACCEPT:
            /*
             * Handle the
             */
            discoverAcceptHandler(httpEntity);
            break;
        case INIT_LEADER_ELECTION:
            /*
             * Handle the election operation
             */
            initLeaderElectionHAndler(httpEntity);
            break;
        case LEADER_ACCEPT:
            /*
             * Handle the acceptation of the leader
             */
            leaderAcceptHandler(httpEntity);
            break;
        case LEADER_REJECT:
            /*
             *  Handle the rejection of the leader
             */
            leaderRejectHandler(httpEntity);
            break;
        case HEART_BEAT:
            /*
             * Handle the health check
             */
            heartBeatHAndler(httpEntity);
            break;
        case MEMORY_STATUS:
            /*
             * Handle the memory check: If the current disk hasn't enough memory
             * he has to transfert all the c
             */
            memoryStatusHandler(httpEntity);
            break;
        default:
            std::cout << "The given protocol is not known" << std::endl;
    }

}

void Node::discoverHandler(HTTPEntity *httpEntity) {
    std::map<string, void *> new_body;
    httpEntity->set(
            DISCOVER_ACCEPT,
            new_body
    );
    httpEntity->send();
}

void Node::discoverAcceptHandler(HTTPEntity *httpEntity) {

}

void Node::initLeaderElectionHAndler(HTTPEntity *httpEntity) {

}

void Node::leaderAcceptHandler(HTTPEntity *httpEntity) {

}

void Node::leaderRejectHandler(HTTPEntity *httpEntity) {

}

void Node::heartBeatHAndler(HTTPEntity *httpEntity) {

}

void Node::memoryStatusHandler(HTTPEntity *httpEntity) {

}