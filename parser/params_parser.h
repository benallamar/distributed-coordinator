//
// Created by Marouane BENALLA on 05/02/2017.
//

#ifndef SERVER_LIBRARY_CONFIG_PARAMS_H

#include <iostream>

#define SERVER_LIBRARY_CONFIG_PARAMS_H

class ConfigParams {
public:
    ConfigParams(int argc, char *argv[]) {
        while (argc--) {
            if (strcmp(argv[argc], "-m") == 0)
                mode = new std::string(argv[argc + 1]);
            if (strcmp(argv[argc], "-p") == 0)
                port = new std::string(argv[argc + 1]);
            if (strcmp(argv[argc], "-h") == 0)
                host = new std::string(argv[argc + 1]);
        }
    }

    bool checkMode(std::string mode) {
        return this->mode->compare(mode) == 0;
    }

    std::string *getPort() {
        return port;
    }

    std::string *getHost() {
        return host;
    }

private:
    std::string *mode, *host, *port;

};

#endif //SERVER_LIBRARY_CONFIG_PARAMS_H
