//
// Created by Marouane BENALLA on 18/02/2017.
//

#ifndef SERVER_LIBRARY_HTTPPARSER_H
#define SERVER_LIBRARY_HTTPPARSER_H

#include <iostream>
#include <map>

using namespace std;

class HTTPParser {
public:
    static map<string, string> parse(char *);

    static map<string, void *> bodyParser(char *);
};


#endif //SERVER_LIBRARY_HTTPPARSER_H
