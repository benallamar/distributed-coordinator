#ifndef LIBSERVER_LIBRARY_H
#define LIBSERVER_LIBRARY_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <mbedtls/ssl.h>
#include <mbedtls/net.h>
#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/debug.h>
#include "http_entity.h"

template<typename Type1, typename Type2>
Type1 getValue(Type1 value, Type2 value2) {
    return value2;
};
namespace ServerAbstraction {

    class Server {
    public:
        Server();

        Server(int, int, int);

        void handshake();


        HTTPEntity* acceptClient();

        int writeSocket(int, std::string *, bool);

        int start(std::string *);

        void enableSSL(bool);

        void enableHTTPAuth(bool);

        void enableOAuth2(bool);

        bool requirHTTPAuth();

        bool requireSSL();

        bool requireOAuth2();

        HTTPEntity *readSocket(int, bool);

        void stop(int);

    protected:
        int domain, type, protocol, back_log = 200;
        mbedtls_net_context sockfd;
        mbedtls_entropy_context entropy;
        mbedtls_ctr_drbg_context ctr_drbg_context;
        mbedtls_ssl_context ssl_context;
        mbedtls_ssl_config ssl_config;
        // For secure connection we could use a certificat
        std::string *cert_path = new std::string("the_path_int_the_system");
        /*
         * This global config params could be customized to ascertain some
         * level of security.
         *      - require_http_auth : the basic http identication using a username &
         *      password
         *      - require_ssl : the TLs layer
         *      - require_oauth_2 : using a token and user id to authenticate
         *      a server.
         */
        bool require_http_auth = false, require_ssl = true, require_oauth2 = true;
    };

    class Client : public Server {
    public:
        int initCall(std::string *, std::string *);

        void sendRequest();

        void broadcast();

        typedef Server super;
    private:
        std::string *host, *port;

    };

}


#endif