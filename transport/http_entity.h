//
// Created by Marouane BENALLA on 15/02/2017.
//

#ifndef SERVER_LIBRARY_HTTPENTITY_H
#define SERVER_LIBRARY_HTTPENTITY_H

#include <iostream>
#include <map>
#include "../serializer/json_serializer.h"
#include "../parser/http_parser.h"

using namespace std;

class HTTPEntity : public Serializer {
public:
    HTTPEntity(char *);

    /*
     * Get the status of the response
     */

    int status();

    /*
     * Resend the request to the server in question
     */
    void send();

    void set(int, map<string, void *> body);

    bool isStatus(const char *);

    bool
    /*
     * Get the response from the socket
     */
    bool isAlive();

    string *getIPAddress();

    string *getHashAddress();

    map<string, void> getBody();

private:
    map<string, void *> body;
    int status_field;
};


#endif //SERVER_LIBRARY_HTTPENTITY_H
