//
// Created by Marouane BENALLA on 05/02/2017.
//
#include "server_abstraction.h"

/*
 * debug mode application
 */
void debug(char *msg) {
    //Display the erros message
    std::cerr << *msg << std::endl;
}

/*
 * Precautions:
 *  The secure trasnport layer'll not be initiated if the configuratin
 *  has not been set up to take in consideration.
 *  But, the limitation for the instance
 */
ServerAbstraction::Server::Server() {
    domain = AF_INET, type = SOCK_STREAM, protocol = 0;
}

ServerAbstraction::Server::Server(int domain, int type, int protocol) {
    this->domain = domain, this->type = type, this->protocol = protocol;
}

int ServerAbstraction::Server::start(std::string *port) {
    int ret;
    /*
     * Initiate the ssl layer
     */
    mbedtls_net_init(&sockfd);
    mbedtls_ssl_init(&ssl_context);
    mbedtls_ssl_config_init(&ssl_config);
    mbedtls_ctr_drbg_init(&ctr_drbg_context);
    mbedtls_entropy_init(&entropy);
    /*
     * Initiate the socket
     */
    if ((ret = mbedtls_ctr_drbg_seed(&ctr_drbg_context,
                                     mbedtls_entropy_func,
                                     &entropy,
                                     (const unsigned char *) pers,
                                     strlen(pers)) != 0) {
        std::cout << "failed,\n mbedtls_ctr_drbg_seed returned" << ret << std::endl;
    }
}

void ServerAbstraction::Server::handshake() {
    /*
     * We start by establishing a consensus between the client
     * and the server. The consensus should be on the key or the certificat.
     */

}


HTTPEntity *ServerAbstraction::Server::acceptClient() {
    /*
     * We accept an incoming communication from the server
     * Here we don't handle the secure layer, just simple transactions
     * In case no sensitive data is circuling in the network, activatint this
     * options could make your transactions more fast.
     */
    struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0)
        debug("Error on the  socket opening");
    return newsockfd;
}

int ServerAbstraction::Server::writeSocket(int client_id, std::string *msg, bool require_ssl_params) {
    /*
     * This methods handle the write operations
     * Either is a clair or secure transaction, both implementation are
     * used here, the user could should choose the options he wants.
     * You could set the options either statically by using the options
     * `require_ssl`or dynamically by setting the params 'require_ssl' to true
     */
    if (require_ssl_params or require_ssl) {
        /*
         * We implement here the secure transport layer
         */

    } else {
        /*
         * Or the normal `clear transport layer`
         */
        int n = write(client_id, msg->c_str(), sizeof(msg->c_str()) * sizeof(char));
        if (n < 0)
            debug("Error on writing on the socket");
    }

}

HTTPEntity *ServerAbstraction::Server::readSocket(int client_id, bool require_ssl_params) {
    /*
     * How does it work ?
     *  The secure layer will not be initiate
     */
    const char *buffer;
    if (require_ssl or require_ssl_params) {
        /*
         * Use the ssl requirement to send information accross
         */
    } else {
        bzero((void *) buffer, 1999);
        if (read(client_id, (void *) buffer, 1999) < 0)
            debug("Error on reading from the socket");

    }
    return new HTTPEntity(buffer);
}

void ServerAbstraction::Server::enableSSL(bool enable) {
    /*
     * Activate statically the secure transport layer
     */
    require_ssl = enable;
}

void ServerAbstraction::Server::enableOAuth2(bool enable) {
    /*
     * Activate statically the secure transport layer
     */
    require_oauth2 = enable;
}

void ServerAbstraction::Server::enableHTTPAuth(bool enable) {
    /*
     * Activate the basic http authentication
     */
    require_http_auth = enable;
}

void ServerAbstraction::Server::requireOAuth2() {
    return require_oauth2;
}

bool ServerAbstraction::Server::requirHTTPAuth() {
    return require_http_auth;
}

bool ServerAbstraction::Server::requireSSL() {
    return require_ssl;
}

void ServerAbstraction::Server::stop(int client_id) {
    close(sockfd);
}

/*
 * Client Class Declaration
 */

int ServerAbstraction::Client::initCall(std::string *host, std::string *port) {
    if (require_ssl) {
        /*
         * The client should initiat a ssl connection with the server
         */
    } else {
        /*
         * A simple connectin could be OK to be done
         */
        struct sockaddr_in serv_addr;
        struct hostent *server;
        char buffer[256];
        sockfd = socket(domain, type, protocol);
        if (sockfd < 0)
            debug("Error to create a socket");
        server = gethostbyname(host->c_str());
        if (server == NULL) {
            debug("Error, no such host" << std::endl;
        }
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;//Ipv4
        bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
        serv_addr.sin_port = htons(atoi(port->c_str()));
        if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
            std::cerr << "Error connection" << std::endl;
        return sockfd;
    }

}