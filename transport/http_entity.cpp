//
// Created by Marouane BENALLA on 15/02/2017.
//

#include "http_entity.h"

HTTPEntity::HTTPEntity(char *http_body) {
    /*
     * Parse the response string
     */
    map<string, string> response = HTTPParser::parse(http_body);
    status_field = stoi(response["status"]);
    body = HTTPParser::bodyParser((char *) response["body"].c_str());
}
