//
// Created by Marouane BENALLA on 18/02/2017.
//

#ifndef SERVER_LIBRARY_OBJECT_SERIALIZER_H
#define SERVER_LIBRARY_OBJECT_SERIALIZER_H

#include <iostream>

class ObjectSerializer {
    /*
     * The aim is to be able to send the objects through the network.
     * Every object'll be serialized (resp. deserialized) in order to be sent
     * (resp. received) using the transport protocol(HTTP)
     * The both static functions (serialize & deserialize should be
     * implemented in straightforwardly way, and they should be secure too.
     */
    static template<class Type1>
    std::string *serialize(Type1 object) {
        /*
         * Serialize an object into JSON format
         */
    }

    static template<class Type1, class Type2>
    Type1 deserialize(Type2 objectClass, std::string *source) {
        /*
         * Generate the appropriate object from the JSON file
         */
    };
};


#endif //SERVER_LIBRARY_OBJECT_SERIALIZER_H
